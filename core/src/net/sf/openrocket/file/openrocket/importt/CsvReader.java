package net.sf.openrocket.file.openrocket.importt;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class CsvReader {
	
	//Para leer de CSV
	String csvFile = "/home/roberto/ValidData.csv";
	BufferedReader br = null;
	String line = "";
	String cvsSplitBy = ",";
	String[] parameters; //Array donde se van a guardar los valores separados del csv
	public RealTimeHandler rthandler;
	
	public CsvReader(RealTimeHandler rthandler) {
		this.rthandler = rthandler;
		try {
			
			br = new BufferedReader(new FileReader(csvFile));
			
			while ((line = br.readLine()) != null) {
				
				this.rthandler.addPacket(line);
				
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		System.out.println("CSV EOF! DONE!");
		
		
	}
}

