package net.sf.openrocket.file.openrocket.importt;


import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;



public class RTGraph extends JFrame {
	
	//private static final long serialVersionUID = 1L;
	
	XYSeriesCollection dataset;
	
	
	// Constructor resive: Titulo de la Ventana, Titulo del Grafico, Titulo Eje X, Titulo Eje Y, Arreglo con los nombres de los parametros
	public RTGraph(String applicationTitle, String chartTitle, String xTitle, String yTitle, String parametersList[]) {
		
		super(applicationTitle);
		
		//Create empty data set, con la lista ne nombres
		dataset = createDataset(parametersList);
		
		// based on the dataset we create the chart
		JFreeChart lineChart = ChartFactory.createXYLineChart(chartTitle, xTitle, yTitle, dataset, PlotOrientation.VERTICAL, true, true, false);
		
		// Adding chart into a chart panel
		ChartPanel chartPanel = new ChartPanel(lineChart);
		
		// settind default size
		chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
		
		// add to contentPane
		setContentPane(chartPanel);
	}
	
	private XYSeriesCollection createDataset(String parametersList[]) {
		
		dataset = new XYSeriesCollection();
		
		//Para cada nombre en la lista se crea un nuevo XYSeries y se agrega al dataset
		for (int i = 0; i < parametersList.length; i++) {
			dataset.addSeries(new XYSeries(parametersList[i]));
		}
		
		
		
		return dataset;
		
	}
	
	public void addValueToSeries(String seriesName, double x, double y) {
		dataset.getSeries(seriesName).add(x, y);
		
	}
	
	
	
	
	
}
