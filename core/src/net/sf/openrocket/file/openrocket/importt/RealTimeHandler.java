package net.sf.openrocket.file.openrocket.importt;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.openrocket.simulation.FlightDataBranch;
import net.sf.openrocket.simulation.FlightDataType;
import net.sf.openrocket.simulation.FlightEvent;
import net.sf.openrocket.unit.UnitGroup;

//import net.sf.openrocket.simulation.FlightEvent.Type;

public class RealTimeHandler {
	@SuppressWarnings("unused")
	private final FlightDataType[] types;
	private final FlightDataBranch branch;
	private Double initialAltitude = null;
	
	private static final Logger log = LoggerFactory.getLogger(RealTimeHandler.class);
	
	String namesListAcceleration[] = { "X", "Y", "Z" };
	RTGraph chartAcceleration = new RTGraph("Acceleration Window", "Acceleration", "flight time (seconds)", "acceleration (m/s2)", namesListAcceleration);
	
	String namesListAltitude[] = { "altitude" };
	RTGraph chartAltitude = new RTGraph("Altitude Window", "Altitude", "flight time (seconds)", "altitude (meters)", namesListAltitude);
	
	public RealTimeHandler(String name, String typeList) {
		
		String[] split = typeList.split(",");
		types = new FlightDataType[split.length];
		for (int i = 0; i < split.length; i++) {
			String typeName = split[i];
			FlightDataType matching = findFlightDataType(typeName);
			types[i] = matching;
			types[i] = FlightDataType.getType(typeName, matching.getSymbol(), matching.getUnitGroup());
		}
		
		
		chartAcceleration.pack();
		chartAcceleration.setVisible(true);
		//chartGyroscope.pack();
		//chartGyroscope.setVisible(true);
		chartAltitude.pack();
		chartAltitude.setVisible(true);
		
		// TODO: LOW: May throw an IllegalArgumentException
		branch = new FlightDataBranch(name, types);
	}
	
	// Find the full flight data type given name only
	// Note: this way of doing it requires that custom expressions always come before flight data in the file,
	// not the nicest but this is always the case anyway.
	private FlightDataType findFlightDataType(String name) {
		
		// Kevins version with lookup by key. Not using right now
		/*
		if ( key != null ) {
			for (FlightDataType t : FlightDataType.ALL_TYPES){
				if (t.getKey().equals(key) ){
					return t;
				}
			}
		}
		*/
		
		// Look in built in types
		for (FlightDataType t : FlightDataType.ALL_TYPES) {
			if (t.getName().equals(name)) {
				return t;
			}
		}
		/*
		// Look in custom expressions
		for (CustomExpression exp : simHandler.getDocument().getCustomExpressions()) {
			if (exp.getName().equals(name)) {
				return exp.getType();
			}
		}
		*/
		log.warn("Could not find the flight data type '" + name + "' used in the XML file. Substituted type with unknown symbol and units.");
		return FlightDataType.getType(name, "Unknown", UnitGroup.UNITS_NONE);
	}
	
	/**
	 * @param timeToOptimumAltitude
	 * @see net.sf.openrocket.simulation.FlightDataBranch#setTimeToOptimumAltitude(double)
	 */
	public void setTimeToOptimumAltitude(double timeToOptimumAltitude) {
		branch.setTimeToOptimumAltitude(timeToOptimumAltitude);
	}
	
	/**
	 * @param optimumAltitude
	 * @see net.sf.openrocket.simulation.FlightDataBranch#setOptimumAltitude(double)
	 */
	public void setOptimumAltitude(double optimumAltitude) {
		branch.setOptimumAltitude(optimumAltitude);
	}
	
	public FlightDataBranch getBranch() {
		branch.immute();
		return branch;
	}
	
	public double getMax(FlightDataType type) {
		return branch.getMaximum(type);
	}
	
	public void addPacket(String content) {
		// Check line format
		String[] split = content.split(",");
		if (split.length != types.length) {
			//warnings.add("Data point did not contain correct amount of values, ignoring point.");
			return;
		}
		// Parse the doubles
		double[] values = new double[split.length];
		for (int i = 0; i < values.length; i++) {
			try {
				values[i] = DocumentConfig.stringToDouble(split[i]);
				if (i == 1) {
					values[i] /= 1000000;
				}
				
			} catch (NumberFormatException e) {
				System.out.println(e);
				//warnings.add("Data point format error, ignoring point.");
				return;
			}
			
		}
		chartAcceleration.addValueToSeries("X", values[1], values[2]);
		chartAcceleration.addValueToSeries("Y", values[1], values[3]);
		chartAcceleration.addValueToSeries("Z", values[1], values[4]);
		
		//double altitude = ((1 - Math.pow(values[7] / 1013.25, 0.190284)) * 145366.45) * 0.3048;
		if (this.initialAltitude == null) {
			this.initialAltitude = values[7];
		}
		values[7] = values[7] - this.initialAltitude;
		chartAltitude.addValueToSeries("altitude", values[1], values[7]);
		// Add point to branch
		branch.addPoint();
		for (int i = 0; i < types.length; i++) {
			branch.setValue(types[i], values[i]);
		}
	}
	
	private double timeLaunch = -1;
	private double timeToBurnout = -1;
	private double timeToApogee = -1;
	private double timeToHitGround = -1;
	
	public double getTimeLaunch() {
		return timeLaunch;
	}
	
	public double getTimeToBurnout() {
		return timeToBurnout - timeLaunch;
	}
	
	public double getTimeToApogee() {
		return timeToApogee - timeLaunch;
	}
	
	public double getFlightTime() {
		return timeToHitGround - timeLaunch;
	}
	
	public double getApogee() {
		FlightDataType altitudeType = FlightDataType.TYPE_ALTITUDE;
		List<Double> altitudeValues = branch.get(altitudeType);
		return this.getMax(altitudeType) - altitudeValues.get(0);
	}
	
	public void setEvents() {
		FlightDataType altitudeType = FlightDataType.TYPE_ALTITUDE;
		FlightDataType timeType = FlightDataType.TYPE_TIME;
		FlightDataType VAType = FlightDataType.TYPE_ACCELERATION_Z;
		List<Double> altitudeValues = branch.get(altitudeType);
		List<Double> timeValues = branch.get(timeType);
		List<Double> vaValues = branch.get(VAType);
		double maxAltitude = branch.getMaximum(altitudeType);
		int index = altitudeValues.indexOf(maxAltitude);
		timeToApogee = timeValues.get(index);
		timeToHitGround = timeValues.get(timeValues.size() - 1);
		branch.addEvent(new FlightEvent(net.sf.openrocket.simulation.FlightEvent.Type.APOGEE, timeToApogee));
		branch.addEvent(new FlightEvent(net.sf.openrocket.simulation.FlightEvent.Type.GROUND_HIT, timeToHitGround));
		int state = 0;
		for (int i = 0; i < vaValues.size() && state < 2; ++i) {
			switch (state) {
			case 0:
				if (vaValues.get(i) > 15) {
					timeLaunch = timeValues.get(i);
					++state;
					branch.addEvent(new FlightEvent(net.sf.openrocket.simulation.FlightEvent.Type.LAUNCH, timeLaunch));
				}
				break;
			case 1:
				if (vaValues.get(i) < 0) {
					timeToBurnout = timeValues.get(i);
					++state;
					branch.addEvent(new FlightEvent(net.sf.openrocket.simulation.FlightEvent.Type.BURNOUT, timeToBurnout));
					
				}
				break;
			}
		}
		
		
	}
}