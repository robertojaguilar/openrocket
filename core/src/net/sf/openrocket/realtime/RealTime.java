package net.sf.openrocket.realtime;

import java.util.Date;

import net.sf.openrocket.document.Simulation;
import net.sf.openrocket.document.Simulation.Status;
import net.sf.openrocket.file.openrocket.importt.CsvReader;
import net.sf.openrocket.file.openrocket.importt.RealTimeHandler;
import net.sf.openrocket.file.openrocket.importt.SerialReader;
import net.sf.openrocket.rocketcomponent.Rocket;
import net.sf.openrocket.simulation.DefaultSimulationOptionFactory;
import net.sf.openrocket.simulation.FlightData;
import net.sf.openrocket.simulation.FlightDataType;
import net.sf.openrocket.simulation.SimulationOptions;
import net.sf.openrocket.startup.Application;



public class RealTime {
	
	private Simulation simulation = null;
	SerialReader main;
	private CsvReader reader;
	public RealTimeHandler rthandler;
	
	public RealTime(Rocket rocket) {
		
		SimulationOptions options = new SimulationOptions(rocket);
		DefaultSimulationOptionFactory f = Application.getInjector().getInstance(DefaultSimulationOptionFactory.class);
		options.copyConditionsFrom(f.getDefault());
		
		options.setMotorConfigurationID(rocket.getDefaultConfiguration().getFlightConfigurationID());
		Status status = Status.EXTERNAL;
		
		Date date = new Date();
		String flightName = "Flight" + System.currentTimeMillis();
		this.simulation = new Simulation(
				rocket,
				status, flightName,
				options,
				null,
				null);
		
		//String typeList = "FlightID,PacketID,Time,Axis X acceleration,Axis Y acceleration,Vertical acceleration,Roll Rate,Pitch Rate,Yaw Rate,Magnitude X,Magnitude Y,Magnitude Z,Latitude,Longitude,Altitude,Air temperature,Air pressure";
		String typeList = "PacketID,Time,Axis X acceleration,Axis Y acceleration,Vertical acceleration,Latitude,Longitude,Altitude";
		this.rthandler = new RealTimeHandler(flightName, typeList);
	}
	
	public void start() {
		
		this.main = new SerialReader(this.rthandler);
		try {
			run();
		} catch (Exception e) {
			
		}
		
	}
	
	public void startCsv() {
		this.reader = new CsvReader(this.rthandler);
	}
	
	public void stop() {
		if (this.main != null) {
			this.main.close();
		}
		this.saveFlight();
	}
	
	public void saveFlight() {
		FlightDataType typeAcceleration = FlightDataType.TYPE_ACCELERATION_Z;
		double maxAcceleration = this.rthandler.getMax(typeAcceleration);
		
		
		double maxAltitude = this.rthandler.getApogee();
		this.rthandler.setEvents();
		
		FlightData flightData = new FlightData(maxAltitude, 0.0, maxAcceleration, 0.0, this.rthandler.getTimeToApogee(), this.rthandler.getFlightTime(), 0.0, 0.0, this.rthandler.getTimeToBurnout());
		flightData.addBranchRealFlight(this.rthandler.getBranch());
		this.simulation.setFlightData(flightData);
	}
	
	public void run() throws Exception {
		this.main.initialize();
		Thread t = new Thread() {
			public void run() {
				//the following line will keep this app alive for 1000    seconds,
				//waiting for events to occur and responding to them    (printing incoming messages to console).
				try {
					Thread.sleep(1000000);
				} catch (InterruptedException ie) {
				}
			}
		};
		t.start();
		System.out.println("Started");
	}
	
	public Simulation getSimulation() {
		return this.simulation;
	}
	
}
